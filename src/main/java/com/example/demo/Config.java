package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.Queue;

@Configuration
public class Config {
    @Bean
    public Queue<String> a() {
        return new F<String>();
    }

    @Bean
    @DependsOn("d")
    public Object c(Queue<String> a) {
        return new Object();
    }
}
